import Home from "./pages/Home";
import MyCities from "./pages/MyCities";
import NotFound from "./pages/NotFound";
import Search from "./pages/Search";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<MyCities />} />
        <Route exact path="/search" element={<Search />} />
        <Route exact path="/city/:name" element={<Home />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
}

export default App;
