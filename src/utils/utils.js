const MILLISEC_RATE = 1000;
const SECS_IN_ONE_HOUR = 3600;

export const getHoursAndMinutes = (timestamp, timezone) => {
  if (!timestamp) return;

  const timestampAsDate = createDateFromTimestamp(timestamp, timezone);
  const hours = formatTime(timestampAsDate.getHours());
  const minutes = formatTime(timestampAsDate.getMinutes());

  return `${hours}:${minutes}`;
};

const createDateFromTimestamp = (timestamp, timezone) => {
  const timestampAsInteger = parseInt(timestamp);
  const timezoneAsInteger = parseInt(timezone);

  const calculatedTime =
    timestampAsInteger * MILLISEC_RATE +
    (timezoneAsInteger * MILLISEC_RATE - SECS_IN_ONE_HOUR * MILLISEC_RATE);

  return new Date(calculatedTime);
};

const formatTime = (time) => {
  return time < 10 ? `0${time}` : time;
};

export const roundNumber = (number) => {
  if (!number) return;

  return Math.round(parseInt(number));
};
