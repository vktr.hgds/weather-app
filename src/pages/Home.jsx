import WeatherItem from "../components/WeatherItem";
import NavBar from "../components/NavBar";

const Home = () => {
  return (
    <>
      <NavBar />
      <Weather />
    </>
  );
};

export default Home;
