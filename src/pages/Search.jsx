import CitySearch from "../components/CitySearch";
import NavBar from "../components/NavBar";

const Search = () => {
  return (
    <>
      <NavBar />
      <CitySearch />
    </>
  );
};

export default Search;
