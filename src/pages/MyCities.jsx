import CityList from "../components/CityList";

const MyCities = () => {
  return (
    <>
      <CityList />
    </>
  );
};

export default MyCities;
