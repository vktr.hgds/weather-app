import NotFoundPage from "../components/NotFoundPage";
import NavBar from "../components/NavBar";

const NotFound = () => {
  return (
    <>
      <NavBar />
      <NotFoundPage />
    </>
  );
};

export default NotFound;
