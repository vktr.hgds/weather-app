import styled from "styled-components";
import React, { useEffect, useState } from "react";
import CityListItem from "./CityListItem";
import Aos from "aos";
import "aos/dist/aos.css";
import axios from "axios";

const CityList = () => {
  const [savedCities, setSavedCities] = useState(
    JSON.parse(localStorage.getItem("cities")) || []
  );
  const [isDataCollected, setIsDataCollected] = useState(false);

  // effect/animation
  useEffect(() => {
    Aos.init({
      mirror: false,
    });
  }, []);

  // get cities by open API
  useEffect(() => {
    const getAllSavedCities = async () => {
      try {
        if (savedCities.length === 0) {
          const response = await axios.get(
            `${process.env.REACT_APP_API_BASE_URL}weather?q=Budapest&appid=${process.env.REACT_APP_API_KEY}&units=metric`
          );
          setSavedCities(response.data);
          localStorage.setItem("cities", JSON.stringify([response.data]));
        } else {
          const localStorageCities = JSON.parse(localStorage.getItem("cities"));
          setSavedCities(localStorageCities);
        }
        setIsDataCollected(true);
      } catch (err) {}
    };

    getAllSavedCities();
  }, [isDataCollected]);

  return (
    <Container>
      <Wrapper data-aos="fade-left" data-aos-duration="1100">
        <CityListItem cities={savedCities} />
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  padding: 20px;
  display: flex;
  height: 100vh;
  flex-direction: column;
  background-color: #1e2431;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  height: inherit;
`;

export default CityList;
