import React from "react";
import styled from "styled-components";
import { Add } from "@material-ui/icons";
import { Link } from "react-router-dom";

const CityListItem = ({ cities }) => {
  return (
    <Container>
      <Rectangle>
        {cities?.length > 0 &&
          cities.map((item) => (
            <Link
              to={`/city/${item?.name}`}
              style={{ textDecoration: "none", padding: "5px" }}
              key={item.id}
            >
              <RectangleInfo
                size="40"
                color="#96b8cb"
                paddingBottom="20"
                weight="600"
              >
                {item?.name}
              </RectangleInfo>
            </Link>
          ))}

        <Link
          to="/search"
          style={{ textDecoration: "none", paddingTop: "10px" }}
        >
          <Add
            style={{ fontSize: "40px", fontWeight: 600, color: "#47fe53" }}
          />
        </Link>
      </Rectangle>
    </Container>
  );
};

const Container = styled.div`
  padding: 20px;
  flex: 1;
  min-width: 280px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`;

const Rectangle = styled.div`
  position: absolute;
  z-index: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const RectangleInfo = styled.span`
  font-size: ${(props) => props.size}px;
  padding: ${(props) => (props.padding ? props.padding : 0)}px;
  padding-bottom: ${(props) =>
    props.paddingBottom ? props.paddingBottom : 0}px;
  color: ${(props) => (props.color ? props.color : "#fff")};
  font-weight: ${(props) => (props.weight ? props.weight : 300)};
`;

export default CityListItem;
