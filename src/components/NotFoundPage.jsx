import React from "react";
import styled from "styled-components";
import { SentimentVeryDissatisfiedSharp } from "@material-ui/icons";

const NotFoundPage = () => {
  return (
    <Container>
      <NotFoundStatus>404</NotFoundStatus>
      <NotFoundMessage>This page could not be found.</NotFoundMessage>
    </Container>
  );
};

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const NotFoundStatus = styled.span`
  font-size: 60px;
  padding: 20px;

  @media screen and (max-width: 400px) {
    font-size: 24px;
  }
`;

const NotFoundMessage = styled.span`
  font-size: 40px;

  @media screen and (max-width: 400px) {
    font-size: 24px;
  }
`;

export default NotFoundPage;
