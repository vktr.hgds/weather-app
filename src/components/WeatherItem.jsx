import React from "react";
import styled from "styled-components";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import "weather-icons/css/weather-icons.css";
import Aos from "aos";
import "aos/dist/aos.css";
import { getHoursAndMinutes, roundNumber } from "../utils/utils";

const WeatherItem = () => {
  const [weather, setWeather] = useState(null);
  const { name } = useParams();
  const currentHour = null;
  const currentMinute = null;

  if (weather) {
    currentHour = getHoursAndMinutes(weather?.dt, weather?.timezone)?.split(
      ":"
    )[0];
    currentMinute = getHoursAndMinutes(weather?.dt, weather?.timezone)?.split(
      ":"
    )[1];
  }

  // effect/animation
  useEffect(() => {
    Aos.init({
      mirror: false,
    });
  }, []);

  // get weather/city info by open API
  useEffect(() => {
    const getWeather = async () => {
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_API_BASE_URL}weather?q=${name}&appid=${process.env.REACT_APP_API_KEY}&units=metric`
        );

        setWeather(response.data);
      } catch (err) {}
    };

    getWeather();
  }, []);

  const getDescription = (weatherArr) => {
    if (!weatherArr) return "";

    return weatherArr[0].description;
  };

  const getIcon = (weather) => {
    if (!weather || !weather.weather) return "";

    const iconCode = weather?.weather[0].icon;

    return "http://openweathermap.org/img/w/" + iconCode + ".png";
  };

  return (
    <Container>
      <ItemContainer>
        <Wrapper>
          {weather && (
            <Rectangle>
              <RectangleInfo size="50" color="#337fba">
                {currentHour}
              </RectangleInfo>
              <RectangleInfo size="50" color="#337fba">
                {currentMinute}
              </RectangleInfo>
              <RectangleInfo
                size="30"
                color="#96b8cb"
                paddingBottom="50"
                weight="600"
              >
                {weather?.name}
              </RectangleInfo>
              <RectangleInfoTable direction="column">
                <IconImage src={getIcon(weather)} />
                <RectangleInfo size="12" color="#96b8cb" paddingBottom="20">
                  {getDescription(weather?.weather)}
                </RectangleInfo>
              </RectangleInfoTable>
              <RectangleInfoTable>
                <RectangleInfo
                  className="wi wi-thermometer"
                  style={{
                    color: "#0c3a74",
                    flex: 1,
                    fontSize: 25,
                    paddingLeft: 10,
                  }}
                />
                <RectangleInfo size="30" color="#2e6076" style={{ flex: 1 }}>
                  {roundNumber(weather?.main?.temp)} C°
                </RectangleInfo>
              </RectangleInfoTable>
              <RectangleInfoTable>
                <RectangleInfo
                  className="wi wi-sunrise"
                  style={{ color: "#0c3a74", flex: 1, fontSize: 25 }}
                />
                <RectangleInfo size="30" color="#2e6076" style={{ flex: 1 }}>
                  {getHoursAndMinutes(weather?.sys?.sunrise)}
                </RectangleInfo>
              </RectangleInfoTable>
              <RectangleInfoTable>
                <RectangleInfo
                  className="wi wi-sunset"
                  style={{ color: "#0c3a74", flex: 1, fontSize: 25 }}
                />
                <RectangleInfo size="30" color="#2e6076" style={{ flex: 1 }}>
                  {getHoursAndMinutes(weather?.sys?.sunset)}
                </RectangleInfo>
              </RectangleInfoTable>
            </Rectangle>
          )}
        </Wrapper>
      </ItemContainer>
    </Container>
  );
};

const Container = styled.div`
  padding: 20px;
  display: flex;
  height: 100%;
  flex-direction: column;
  background-color: #1e2431;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const ItemContainer = styled.div`
  padding: 20px;
  flex: 1;
  min-width: 280px;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: top;
  position: relative;
`;

const Rectangle = styled.div`
  position: absolute;
  z-index: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const RectangleInfo = styled.span`
  font-size: ${(props) => props.size}px;
  padding: ${(props) => (props.padding ? props.padding : 0)}px;
  padding-bottom: ${(props) =>
    props.paddingBottom ? props.paddingBottom : 0}px;
  color: ${(props) => (props.color ? props.color : "#fff")};
  font-weight: ${(props) => (props.weight ? props.weight : 300)};
`;

const RectangleInfoTable = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 180px;
  padding: 0;
  margin: 10px 0;
  flex-direction: ${(props) => (props.direction ? props.direction : "row")};
`;

const IconImage = styled.img`
  width: 70px;
  height: 70px;
  margin-bottom: 10px;
`;

export default WeatherItem;
