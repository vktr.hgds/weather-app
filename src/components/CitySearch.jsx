import styled from "styled-components";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Aos from "aos";
import "aos/dist/aos.css";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

const CitySearch = () => {
  const [searchedCities, setSearchedCities] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [chosenCity, setChosenCity] = useState(null);
  const MAX_RESULT = 8;

  // effect/animation
  useEffect(() => {
    Aos.init({
      mirror: false,
    });
  }, []);

  // get weather by open API
  useEffect(() => {
    const getCities = async () => {
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_API_BASE_URL}weather?q=${searchValue}&appid=${process.env.REACT_APP_API_KEY}&units=metric`
        );

        getValidCities(response, searchedCities);
        console.log(response.data);
      } catch (err) {}
    };

    getCities();
  }, [searchValue, chosenCity]);

  const getValidCities = (response, searchedCities) => {
    const currentCity = response.data;
    const cityIds = allCityIds(searchedCities, localStorage.getItem("cities"));
    const uniqueCities = createUniqueCitiesList(cityIds, currentCity);

    setSearchedCities(uniqueCities);
  };

  const allCityIds = (searchedCities, localStorageCities) => {
    const searchedCityIds = getCityIds(searchedCities);
    const localStorageCityIds = getCityIds(JSON.parse(localStorageCities));

    return [...new Set([...searchedCityIds, ...(localStorageCityIds || [])])];
  };

  const getCityIds = (arr) => arr?.map((item) => item.id);

  const createUniqueCitiesList = (cityIds, currentCity) => {
    return cityIds.includes(currentCity.id)
      ? [...searchedCities]
      : [...searchedCities, currentCity];
  };

  const handleChange = (e) => {
    setSearchValue(e.target.value);
  };

  const handleOnclick = (clickedCity) => {
    const selectedCity = searchedCities.find(
      (city) => city?.id === clickedCity?.id
    );

    setChosenCity({ ...selectedCity });
  };

  const saveCityToLocalStorage = (e) => {
    if (!chosenCity) return;

    let previousCities = JSON.parse(localStorage.getItem("cities")) || [];
    let isCityPresent = false;

    previousCities.forEach((item) => {
      if (item.id === chosenCity.id) isCityPresent = true;
    });

    if (isCityPresent) return;

    previousCities.push(chosenCity);

    localStorage.setItem("cities", JSON.stringify(previousCities));
  };

  const getAllFilteredCities = () => {
    let filteredCities = searchedCities.filter((city) =>
      city.name.toLowerCase().includes(searchValue.toLowerCase())
    );

    return filteredCities.length > MAX_RESULT
      ? filteredCities.slice(0, MAX_RESULT)
      : filteredCities;
  };

  const getColoredText = (item) => {
    let cityName = item?.name;

    if (
      !cityName ||
      !cityName?.toLowerCase().includes(searchValue?.toLowerCase())
    )
      return;

    const parts = cityName.split(new RegExp(`(${searchValue})`, "gi"));

    return (
      <span>
        {parts.map((part, i) => (
          <span
            key={i}
            style={
              part.toLowerCase() === searchValue.toLowerCase()
                ? { color: "#96b8cb" }
                : { color: "#337fba" }
            }
          >
            {part}
          </span>
        ))}
      </span>
    );
  };

  return (
    <Container>
      <Wrapper data-aos="fade-left" data-aos-duration="1100">
        <SearchContainer>
          <SearchInput onChange={handleChange} />
          {getAllFilteredCities().length > 0 && (
            <KeyboardArrowDownIcon
              style={{
                color: "#96b8cb",
                fontSize: 40,
              }}
            />
          )}
        </SearchContainer>

        <Rectangle>
          {getAllFilteredCities().map((item) => (
            <div key={item?.id}>
              <InfoButton
                size="40"
                color="#96b8cb"
                paddingBottom="10"
                weight="600"
                data-value={item}
                value={item.name}
                onClick={() => handleOnclick(item)}
              >
                {getColoredText(item)}
              </InfoButton>
            </div>
          ))}
        </Rectangle>
      </Wrapper>
      {chosenCity && (
        <SaveButton onClick={saveCityToLocalStorage}>SAVE</SaveButton>
      )}
    </Container>
  );
};

const Container = styled.div`
  padding: 20px;
  padding-top: 0px;
  display: flex;
  height: 100vh;
  flex-direction: column;
  background-color: #1e2431;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  height: inherit;
  flex-direction: column;
`;

const SearchContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 30px;
  background-color: inherit;
  border: none;
  border-bottom: 1px solid #337fba;
  margin-bottom: 30px;
  width: 300px;
  transition: 0.1s ease all;

  &:focus {
    outline: none;
    border-bottom: 2px solid #337fba;
  }
`;

const SearchInput = styled.input`
  background-color: inherit;
  color: #96b8cb;
  font-size: 30px;
  border: none;
  width: 260px;

  &:focus {
    outline: none;
  }
`;

const Rectangle = styled.div`
  z-index: 0;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
`;

const InfoButton = styled.button`
  font-size: ${(props) => props.size}px;
  padding: ${(props) => (props.padding ? props.padding : 0)}px;
  padding-bottom: ${(props) =>
    props.paddingBottom ? props.paddingBottom : 0}px;
  color: ${(props) => (props.color ? props.color : "#fff")};
  font-weight: ${(props) => (props.weight ? props.weight : 300)};
  border: none;
  background-color: inherit;
  cursor: pointer;

  &:active {
    color: white;
  }
`;

const SaveButton = styled.button`
  position: absolute;
  bottom: 0;
  right: 0;
  margin: 20px;
  background-color: inherit;
  border: none;
  color: #4494af;
  font-size: 20px;
  font-weight: bold;
  transition: 0.2s all ease;
  cursor: pointer;

  & :active,
  :hover {
    color: white;
  }
`;

export default CitySearch;
