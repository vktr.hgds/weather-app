import styled from "styled-components";
import React from "react";
import { ArrowBackIos } from "@material-ui/icons";
import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <Container>
      <Wrapper>
        <Link to="/" style={{ textDecoration: "none", color: "#337fba" }}>
          <ArrowBackIos style={{ fontSize: "30px", paddingLeft: "20px" }} />
        </Link>
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  padding: 10px;
  height: 30px;
  flex-direction: column;
  background-color: #1e2431;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export default NavBar;
